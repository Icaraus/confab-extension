// @ts-nocheck
let contextMenuItem = {
    "id": "collect",
    "title": "Export to CONFAB",
    "contexts": ["selection"]
};

chrome.contextMenus.create(contextMenuItem);

chrome.contextMenus.onClicked.addListener((clickData) => {
    var value = clickData.selectionText;

    if (clickData.menuItemId === "collect" && value) {

        chrome.tabs.getSelected(null, function(tab) {
            var tablink = tab.url;

            chrome.storage.local.set({ 'text': value, 'URL': tablink }, () => {})

            chrome.storage.local.get(null, function(items) {
                var result = JSON.stringify(items);

                var url = 'data:application/json;base64,' + btoa(result);
                chrome.downloads.download({
                    url: url,
                    filename: 'confab.json'
                });
            });

        });
    }
});
